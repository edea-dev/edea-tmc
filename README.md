# EDeA TMC

edea-tmc is a small library for test automation with [edea-ms](https://gitlab.com/edea-dev/edea-ms).
See [edea-tmc-test.ipynb](./edea-tmc-test.ipynb) for a small example how to use it.
